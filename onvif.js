const onvif = require("node-onvif");

// Function to scan the network for ONVIF cameras
const scanCameras = async (user, pass) => {
  console.log("Scanning the network for ONVIF cameras...");

  // Ищем камеры в сети по протоколу ONVIF
  const devices = await onvif.startProbe();

  if (devices.length === 0) {
    return { success: false, message: 'No ONVIF cameras found.' };
  }

  let message = 'ONVIF devices found:\n\n';
  const cameras = [];

  for (const device of devices) {
    try {
      console.log(`Found device: ${device.urn}`);
      const camera = new onvif.OnvifDevice({
        xaddr: device.xaddrs[0],
        user : user,
        pass : pass
      });
      await camera.init();
      const url = camera.rtspUri;
      message += `${device.urn}: ${url}\n`;
      console.log(`RTSP link: ${url}\n`);
      cameras.push({ urn: device.urn, url });
    } catch (error) {
      console.error(`Error retrieving RTSP link for device ${device.urn}: ${error}\n`);
      message += `Error retrieving RTSP link for device ${device.urn}: ${error}\n`;
    }
  }

  return { success: true, message, cameras };
};
module.exports = scanCameras;

